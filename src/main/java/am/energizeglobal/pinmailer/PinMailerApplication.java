package am.energizeglobal.pinmailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PinMailerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PinMailerApplication.class, args);
    }

}
