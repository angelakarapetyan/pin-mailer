package am.energizeglobal.pinmailer.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

@Service
public class PINMailerService {

    @Value("${adyton.ip}")
    private String adytonIp;
    @Value("${adyton.port}")
    private int adytonPort;

    public String encodeData(String data, String algorithmId) {
        String command = String.format("%s%s%s%s%s%s%s%s%s%s%s%s",
                HSMCommand.EDP_MAGIC_NUMBER,
                HSMCommand.EDP_HEADER_VERSION,
                HSMCommand.EDP_DESTINATION_APPLICATION_ADDRESS,
                HSMCommand.EDP_DESTINATION_DEVICE_ADDRESS_1,
                HSMCommand.EDP_HOST_MESSAGE_ID_LENGTH,
                HSMCommand.START_INDICATOR,
                HSMCommand.PCI_DATA, data,
                HSMCommand.PCI_DATA_ALGORITHM_ID, algorithmId,
                HSMCommand.ENCRYPT_PCI_DATA,
                HSMCommand.ENCRYPTED_PCI_DATA);

        String response = sendRequestToHSM(command);
        String encryptedData = response.split(HSMCommand.ENCRYPTED_PCI_DATA)[1];

        return encryptedData;
    }

    public String decodeData(String data) {
        String command = String.format("%s%s%s%s%s%s%s%s%s%s",
                HSMCommand.EDP_MAGIC_NUMBER,
                HSMCommand.EDP_HEADER_VERSION,
                HSMCommand.EDP_DESTINATION_APPLICATION_ADDRESS,
                HSMCommand.EDP_DESTINATION_DEVICE_ADDRESS_1,
                HSMCommand.EDP_HOST_MESSAGE_ID_LENGTH,
                HSMCommand.START_INDICATOR,
                HSMCommand.ENCRYPTED_PCI_DATA, data,
                HSMCommand.DECRYPT_PCI_DATA,
                HSMCommand.PCI_DATA);

        String response = sendRequestToHSM(command);
        String decodedData = response.split(HSMCommand.PCI_DATA)[1];

        return decodedData;
    }

    private String sendRequestToHSM(String command) {
        String response = null;
        try (Socket socket = new Socket(adytonIp, adytonPort)) {
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeUTF(command);
            out.flush();

            DataInputStream in = new DataInputStream(socket.getInputStream());
            response = in.readUTF();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
}
