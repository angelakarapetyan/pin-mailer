package am.energizeglobal.pinmailer.service;

public class HSMCommand {

    public static final String START_INDICATOR = "FF";
    public static final String REPLY_CODE = "00";
    public static final String DATA_TAG = "01";
    public static final String COMMAND_TAG = "02";
    public static final String ERROR_TAG = "03";

    public static final String EDP_MAGIC_NUMBER = "FE";
    public static final String EDP_HEADER_VERSION = "30";
    public static final String EDP_DESTINATION_APPLICATION_ADDRESS = "01";
    public static final String EDP_DESTINATION_DEVICE_ADDRESS_1 = "00";
    public static final String EDP_DESTINATION_DEVICE_ADDRESS_2 = "01";
    public static final String EDP_HOST_MESSAGE_ID_LENGTH = "00";

    //input/output data for PCI
    public static final String PCI_DATA = "011D3B00";
    public static final String PCI_DATA_ALGORITHM_ID = "011D3D00";
    public static final String ENCRYPTED_PCI_DATA = "011D3A00";

    //input/output data for BAPOF and EQUENS
    public static final String BAPOF_PIN = "01100000";
    public static final String ISO2 = "01000000";
    public static final String KDEP_VERSION_NR = "01A10000";
    public static final String ENCRYPTED_PIN_KEY_LEFT = "01A10100";
    public static final String ENCRYPTED_PIN_KEY_RIGHT = "01A10200";
    public static final String ENCRYPTED_EQUENS_PIN = "01A10300";
    public static final String BAPOF_KEY_ID = "01100700";
    public static final String PIN_TO_PRINT = "01001D00";

    //commands
    public static final String DECRYPT_BAPOF = "02100000";
    public static final String CREATE_BAPOF = "02100700";
    public static final String PRINT_PIN = "02001600";
    public static final String GEN_BASE = "02100600";
    public static final String DECRYPT_EQUENS_PIN = "02A10000";
    public static final String ENCRYPT_PCI_DATA = "021D1800";
    public static final String DECRYPT_PCI_DATA = "021D1700";
}
