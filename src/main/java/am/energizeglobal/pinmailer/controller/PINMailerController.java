package am.energizeglobal.pinmailer.controller;

import am.energizeglobal.pinmailer.exception.RequestParamMissingException;
import am.energizeglobal.pinmailer.service.PINMailerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class PINMailerController {

    private final PINMailerService pinMailService;

    @Autowired
    PINMailerController(PINMailerService pinMailService) {
        this.pinMailService = pinMailService;
    }

    @PostMapping("/encode")
    public String encodeData(@RequestBody Map<String, String> inputData) {
        String data = inputData.get("data");
        if (StringUtils.isBlank(data)) {
            throw new RequestParamMissingException("data");
        }

        String algorithmId = inputData.get("algorithmId");
        if (StringUtils.isBlank(algorithmId)) {
            throw new RequestParamMissingException("algorithmId");
        }

        return pinMailService.encodeData(data, algorithmId);
    }

    @PostMapping("/decode")
    public String decodeString(@RequestBody String data) {
        if (data.trim().equals("")) {
            throw new RequestParamMissingException("data");
        }

        return pinMailService.decodeData(data);
    }
}
