package am.energizeglobal.pinmailer.exception;

public class RequestParamMissingException extends PINMailerRuntimeException {

    public RequestParamMissingException(String paramName) {
        this(ErrCodes.ERR_PARAM_MISSING, String.format("Param '%s' is required", paramName));
    }

    public RequestParamMissingException(String code, String message) {
        super(code, message);
    }
}