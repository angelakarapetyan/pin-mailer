package am.energizeglobal.pinmailer.exception;

public class PINMailerRuntimeException extends RuntimeException {

    private String code;
    private String message;

    public PINMailerRuntimeException(String code, String message) {
        this.setCode(code);
        this.setMessage(message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
